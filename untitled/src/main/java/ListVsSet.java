import java.util.*;

public class ListVsSet {

    public static void main(String[] args) {
        List<String> liste = new ArrayList<String>();
         liste.add("Susi");
         liste.add("Max");
         liste.add("Susi");
         System.out.println(liste.size());
         System.out.println(liste);

         Set<String> set = new HashSet<String>();
         set.add("Susi");
         set.add("Max");
         set.add("Susi");
        System.out.println(set.size());
        System.out.println(set);

        String[] array = new String[3];
        array[0] = "Susi";
        array[1] = "Max";
        array[2] = "Susi";
        System.out.println(array.length);
        System.out.println(Arrays.toString(array));


    }
}
