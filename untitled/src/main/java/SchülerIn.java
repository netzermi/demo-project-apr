public class SchülerIn {
    private String vorname;
    private int alterJahre;

    public SchülerIn(String vorname, int alterJahre){
        this.vorname = vorname;
        this.alterJahre = alterJahre;
    }

    public String toString(){
        return vorname +  " mit dem Alter: " + alterJahre;
    }

}
