public class Artikel {
    private double preis;
    private String bezeichnung;

    public Artikel(double preis, String bezeichnung) {
        this.preis = preis;
        this.bezeichnung = bezeichnung;
    }


    public String toString() {
        return bezeichnung + " Preis: " + preis;
    }


}
