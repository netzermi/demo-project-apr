import java.util.ArrayList;
import java.util.HashMap;

public class Vorbereitung {

    public static void main(String[] args) {
        String[] csv = {"Vorname,Alter", "Max,15", "Susi,16" };
        ArrayList<SchülerIn> schülerInListe = new ArrayList<>();
        HashMap<Integer,SchülerIn> schülerInMap = new HashMap<>();

        int summe = 0;
        for(int i = 1; i < csv.length; i++){
            String aktuelleZeile = csv[i];
            String[] teile = aktuelleZeile.split(",");
            int alterJahre = Integer.valueOf(teile[1]);
            //summe += alterJahre;
            summe = summe + alterJahre;
            SchülerIn s = new SchülerIn(teile[0],alterJahre);
            schülerInListe.add(s);
            schülerInMap.put(i,s);

        }
       // System.out.println(schülerInListe.toString());
        //System.out.println(summe);
        System.out.println(schülerInMap.get(2));

    }
}
