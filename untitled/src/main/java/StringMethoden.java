import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

public class StringMethoden {


    public static void main(String[] args) {
        //summenBeispiel();
        collectionsBeispiele();
    }

    public static void collectionsBeispiele(){
        String[] csv = {"name,umsatz","PS4,250","PS5,800"};
        List<Artikel> artikelList = new ArrayList<Artikel>();
        HashMap<String,Double> hashMap = new HashMap<String,Double>();

        double minPreis = 250;
        for(int i = 1; i < csv.length; i++){
            String cur = csv[i];
            String[] array = cur.split(",");
            double preis = Double.parseDouble(array[1]);
            if(preis < minPreis){
                minPreis = preis;
            }
            Artikel a = new Artikel(preis, array[0]);
            artikelList.add(a);
            hashMap.put(array[0],preis);
        }



    }

    public static void summenBeispiel() {
        String values = "30;40;15";
        String[] array = values.split(";");
        double sum = 0;
        double min = Double.valueOf(array[0]);
        for (String s : array) {
            double d = Double.valueOf(s);
            sum += d;
            //sum += Double.parseDouble(s);
            if(d < min){
               min = d;
            }
        }
        System.out.println("Summe: " + sum);
        System.out.println("Min: " + min);

    }
}

